from cement import Controller, ex, App
from cement.utils.version import get_version_banner
from cement.core import handler
from onusdarr.controllers.base import Base


class RadarrController(Base):
    class Meta:
        label = 'radarr'
        stacked_on = 'base'
        stacked_type = 'nested'
        description = "These are commands to control the Radarr instance."

    @ex(help="Processes imported movies that are listed as unmonitored")
    def process_unmonitored(self):
        print("Inside process_unmonitored")

        print(self.app.config.get('tmdb', 'api_key'))
        print(self.app.config.get_section('tmdb'))

