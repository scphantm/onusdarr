
from setuptools import setup, find_packages
from onusdarr.core.version import get_version

VERSION = get_version()

f = open('README.md', 'r')
LONG_DESCRIPTION = f.read()
f.close()

setup(
    name='onusdarr',
    version=VERSION,
    description='Management commands for Sonarr and Radarr',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='Willie Slepecki',
    author_email='scphantm@gmail.com',
    url='https://gitlab.com/scphantm/onusdarr',
    license='GNU',
    packages=find_packages(exclude=['ez_setup', 'tests*']),
    package_data={'onusdarr': ['templates/*']},
    include_package_data=True,
    entry_points="""
        [console_scripts]
        onusdarr = onusdarr.main:main
    """,
)
